(function($) {
  "use strict";

  window.URL = window.URL || window.webkitURL;
  navigator.getUserMedia  = navigator.getUserMedia || navigator.webkitGetUserMedia ||
                            navigator.mozGetUserMedia || navigator.msGetUserMedia;

  var video = document.querySelector('video'),
      mediaStream = null,
      recorder = null;

  var record = function(button) {
    console.log(mediaStream);
    recorder = mediaStream.recorder();
  };

  var stop = function(button) {
    mediaStream.stop();
    recorder.getRecordedData(function(blob) {
      console.log(blob);
    });
  };

  var setup = function(stream) {
    video.src = window.URL.createObjectURL(stream);
    mediaStream = stream;
  };

  var error = function(e) {
    $('#rejected').show();
  };

  var not_supported = function() {
    $('#not_supported').show();
  }

  $('#start').click(record);
  $('#stop').click(stop);

  $(function() {
    if (navigator.getUserMedia) {
      navigator.getUserMedia({audio: true, video: true}, setup, error);
    } else {
      alert('Not supported')
    }
  });
}.call(this, jQuery));