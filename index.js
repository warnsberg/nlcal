var express    = require('express');
var app        = express();
var formidable = require('formidable');
var fs         = require('fs');
var events     = require('events');
var knox       = require('knox');
var mpu        = require('knox-mpu');
var slug       = require('slug');


if(!process.env.S3_KEY || !process.env.S3_SECRET || !process.env.S3_BUCKET) {
  console.error('ERROR: Missing config!');
  process.exit(1);
}

var s3Client = knox.createClient({
  key: process.env.S3_KEY,
  secret: process.env.S3_SECRET,
  bucket: process.env.S3_BUCKET,
  region: 'eu-west-1'
});

var FileHandler = function(path, stream){
  var self = this;

  var fstream = new mpu({
    client: s3Client,
    objectName: path,
    stream: stream
  }, function(err, res){
    if(err){
      console.warn('Error when uploading to S3', err);
      self.emit('error'); return;
    }
    self.emit('success');
  });

};

FileHandler.prototype = Object.create(events.EventEmitter.prototype);

app.use(app.router);
app.use('/static', express.static(__dirname + '/static'));
app.set('view engine', 'jade');
app.engine('jade', require('jade').__express);

app.get('/', function(req, res){
  res.render('index');
});

app.post('/upload', function(req, res){
  var form = new formidable.IncomingForm();
  form.onPart = function(part){
    if(!part.filename) {
      form.handlePart(part);
      return;
    }
    var rand = Math.random().toString(36).substring(7);
    var handler = new FileHandler(rand + slug(part.filename), part);
    handler.on('error', function(){
      res.render('error');
    });

    handler.on('success', function(){
      res.render('success');
    })
  };

  form.parse(req, function(err, fields, files){
  });
});

console.log('Listening on ' + (process.argv[2] || 3000));
app.listen(process.argv[2] || 3000);
